export class Game {
  id: bigint;
  name: string;
  createdAt: Date;
  modifiedAt: Date;
}
